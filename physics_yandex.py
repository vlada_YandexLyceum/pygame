import pygame
from pygame.locals import *
from math import sin, cos
from random import randint

try:
    class Circles(pygame.sprite.Sprite):
        def __init__(self, group, x, y, color, r):
            super().__init__(group)
            self.radius = r
            self.image = pygame.Surface((r * 1.6, r * 1.6),
                                        pygame.SRCALPHA, 32)
            self.rect = self.image.get_rect()
            self.rect.x = x - 3
            self.rect.y = y
            self.image.fill(color)


    class Gun(pygame.sprite.Sprite):
        def __init__(self, group, image, x, y):
            super().__init__(group)
            self.const_image = image
            self.image = image
            self.rect = self.image.get_rect()
            self.rect.x = x
            self.rect.y = y

        def update(self, angle, x, y):
            self.image = pygame.transform.rotate(self.const_image, angle)
            self.rect = self.image.get_rect()
            self.rect.center = (x, y)
            pygame.display.flip()



    ############################################программа####################################
    pygame.init()
    pygame.key.set_repeat(1, 70)

    pygame.init()
    x, y = 1300, 800
    (x_display, y_display) = (x, y)
    screen = pygame.display.set_mode((x_display, y_display), 0, 32)
    pygame.display.set_caption("Привет, Физика!")
    FontSise = int(45 / 1500 * x_display)
    clock = pygame.time.Clock()
    gap = int((60 / 1500) * y_display) + y_display // 50  # для осей
    y_display -= y_display // 6  # !!!!!!!!!изменено y поля!
    x_display += x_display // 50
    color_display = (255, 255, 242)


    image = pygame.image.load('gun32.png')
    wheel = pygame.image.load('wheel1.png')
    circles_group = pygame.sprite.Group()
    gun_group = pygame.sprite.Group()
    gun = Gun(gun_group, image, gap, y_display - gap)


    def circle_around_gun_and_gun(xn, yn, angle):
        pygame.draw.circle(screen, color_display, (xn, yn), 70)
        creating_a_coordinate_system()
        circles_group.draw(screen)
        pygame.display.flip()
        gun_group.update(angle, xn, yn)
        gun_group.draw(screen)
        screen.blit(wheel, (xn - 20, yn - 20))


    def get_a_v(x, y, color_display):
        xn, yn = x, y
        x_text_pos, y_text_pos = xn + 70, yn + 55


        angle = 0
        circle_around_gun_and_gun(xn, yn, angle)
        clock = pygame.time.Clock()
        x_v = 0
        y += 20

        color_li = [0, 255, 0]
        n_li = [1, 0]
        k, n = 5, 0

        button_cords = (x_display - 250, y_display, 130, 60)
        drow_rect((0, 0, 100), (x_display - 255, y_display - 5, 140, 70))
        drow_rect((255, 239, 213), button_cords)
        write_text(25, 'Перезапустить', (button_cords[0] + 5, button_cords[1] + 10), (0, 0, 0))

        rect_size = (x_text_pos + x_v, y, 3, 20)
        pygame.draw.rect(screen, color_display, (x_text_pos, y, 400, 80))
        pygame.draw.rect(screen, (0, 255, 0), rect_size)
        screen.blit(pygame.font.SysFont("None", 30).render('v, м/с = ' + str(x_v), 0, (0, 0, 0)),
                    (x_text_pos, y_text_pos))
        screen.blit(pygame.font.SysFont("None", 30).render('a, ° = ' + str(90 - angle), 0, (0, 0, 0)),
                    (x_text_pos, y_text_pos + 30))
        pygame.display.flip()

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    raise SystemExit("Вы хотите закрыть окно?")
                if event.type == pygame.KEYDOWN:
                    rect_size = (x_text_pos + x_v, y, 3, 20)
                    color = (color_li[0], color_li[1], color_li[2])
                    if event.key == pygame.K_RIGHT:
                        if color[0] + k <= 255 and color[1] + k >= 0:
                            x_v += 3
                            color_li[n] += k
                            pygame.draw.rect(screen, color, rect_size)
                            pygame.display.flip()
                    elif event.key == pygame.K_LEFT:
                        if color[1] - k <= 255 and color[0] - k >= 0:
                            x_v -= 3
                            color_li[n] -= k
                            pygame.draw.rect(screen, color_display, rect_size)
                            pygame.display.flip()
                    elif event.key == pygame.K_UP and angle <= 0:
                        circles_group.draw(screen)
                        angle += 1
                        circle_around_gun_and_gun(xn, yn, angle)

                    elif event.key == pygame.K_DOWN and -90 <= angle:
                        circles_group.draw(screen)
                        angle -= 1
                        circle_around_gun_and_gun(xn, yn, angle)

                    if color_li[0] == 255 and color_li[1] == 255:
                        n = n_li[n]
                        k = -k
                    pygame.draw.rect(screen, color_display, (x_text_pos, y_text_pos, 120, 75))
                    screen.blit(pygame.font.SysFont("None", 30).render('v, м/с = ' + str(x_v // 2), 0, (0, 0, 0)),
                                (x_text_pos, y_text_pos))
                    screen.blit(pygame.font.SysFont("None", 30).render('a, ° = ' + str(90 - abs(angle)), 0, (0, 0, 0)),
                                (x_text_pos, y_text_pos + 30))
                    pygame.display.flip()
                elif event.type == pygame.KEYUP and event.key == pygame.K_RETURN:
                    return x_v // 2, 90 - abs(angle)

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    continue_program(event.pos)
            clock.tick(30)
            pygame.display.flip()


    ##################################деления на шкалах########################################
    def drow_rect(color, cords):
        pygame.draw.rect(screen, color, cords)
        pygame.display.flip()


    def continue_program(pos):
        cords = (x_display - 250, y_display, 130, 60)
        if cords[0] < pos[0] < cords[0] + cords[2] and cords[1] < pos[1] < cords[1] + cords[3]:
            circles_group = pygame.sprite.Group()
            cord()


    def write_text(size, text, cords, color=(0, 0, 0)):
        screen.blit(
            pygame.font.SysFont("None", size).render(text, 0, color), cords)


    def x_scale(step, cords_x_line, stop_draw_x_line):
        x_chkala = 0
        a_y = cords_x_line[1][1]
        for a_x in range(cords_x_line[0][0], cords_x_line[1][0], step):
            if x_chkala != 0 and a_x <= stop_draw_x_line - 50:
                if x_chkala / step % 10 == 0:
                    pygame.draw.line(screen, (0, 30, 0), [a_x + x_display // 30, a_y - FontSise // 10],
                                     [a_x + x_display // 30, a_y + FontSise // 6], 3)
                    write_text(int(FontSise * 0.6), str(x_chkala),
                               (a_x + x_display // 48, a_y + y_display // 100))
                else:
                    pygame.draw.line(screen, (0, 0, 0), [a_x + x_display // 30, a_y - FontSise // 10],
                                     [a_x + x_display // 30, a_y + FontSise // 10], 3)
            x_chkala += step
        pygame.display.flip()


    def y_scale(step, cords_x_line, cords_y_line, stop_draw_y_line):
        y_chkala = 0
        const_cord_x = cords_y_line[0][0]
        for cord_y in range(cords_x_line[0][1], cords_y_line[0][1], -step):
            if y_chkala != 0 and cord_y >= stop_draw_y_line:
                if y_chkala / step % 10 == 0:
                    pygame.draw.line(screen, (0, 30, 0), [const_cord_x - x_display // 200, cord_y],
                                     [const_cord_x + x_display // 550, cord_y], 3)
                    write_text(int(FontSise * 0.6), str(y_chkala),
                               (const_cord_x - x_display // 40, cord_y - y_display // 200))
                else:
                    pygame.draw.line(screen, (0, 0, 0), [const_cord_x - x_display // 400, cord_y],
                                     [const_cord_x + x_display // 550, cord_y], 3)
            y_chkala += step
        pygame.display.flip()


    def draw_scale(cords_x_line, cords_y_line):
        step = 10
        x_scale(step, cords_x_line, x_display - gap)
        y_scale(step, cords_x_line, cords_y_line, 30)


    def draw_arrow(first_line_cords, second_line_cords, text, mode):
        screen.blit(pygame.font.SysFont("None", FontSise - 10).render(text, 0, (0, 0, 0)),
                    (mode[0], mode[1]))
        pygame.draw.line(screen, (0, 0, 0), first_line_cords[0], first_line_cords[1],
                         FontSise // 10)
        pygame.draw.line(screen, (0, 0, 0), second_line_cords[0], second_line_cords[1],
                         FontSise // 10)
        pygame.display.flip()


    def draw_point_zero():
        screen.blit(pygame.font.SysFont("None", FontSise).render(str(0), 0, (0, 0, 0)),
                    (gap - FontSise // 2, y_display - gap + 20))
        pygame.draw.circle(screen, (0, 0, 0), (gap, y_display - gap), 5)
        pygame.display.flip()


    def draw_axis(mode):
        pygame.draw.line(screen, (0, 200, 60), mode[0], mode[1],
                         FontSise // 6)
        pygame.display.flip()


    def start_screen(x, y, color_display):
        fon = pygame.transform.scale(pygame.image.load('physics_fon_3.jpg'), (x, y))
        screen.blit(fon, (0, 0))
        drow_rect((0, 0, 100), (20, 235, 1260, 80))
        drow_rect((255, 255, 235), (25, 240, 1250, 70))
        write_text(60, 'Движение полёта тела, брошенного под углом к горизонту', (46, 255), (0, 0, 100))

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    raise SystemExit("Вы хотите закрыть окно?")
                elif event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                    screen.fill(color_display)
                    return  # начинаем игру
            pygame.display.flip()
            clock.tick(10)


    def make_the_circle_fly(x_display, y_display, color_display):
        clock = pygame.time.Clock()
        COLOR = (0, 0, 100)
        sht_x = x_display // 3
        sht_y = 30
        a_color = COLOR
        c = 10

        for little_c in range(c):
            x_cir, y_cir = 0, y_display
            '''V'''
            '''a'''
            v, degrees_a = get_a_v(gap, y_display - gap, color_display)
            clock.tick(30)

            a = (3.14 / 180) * degrees_a
            t = int(v * sin(a) * 2 / 9.8)
            h = int(v ** 2 * (sin(a)) ** 2 / (2 * 9.8))
            s = int(v ** 2 * sin(2 * a) / 9.8)
            sht_y += 40
            write_text(FontSise, 's = {}м,  h(маx) = {}м,  t = {}c,  v = {}м/c,  α = {}°'.format(s, h, t, v, degrees_a),
                       (sht_x, sht_y - int(sht_y * 0.2)), COLOR)
            indik = True
            for i in range(0, t + 1):
                for j in range(100):
                    i += 0.01
                    for j in pygame.event.get():
                        if j.type == QUIT:
                            raise SystemExit("Вы хотите закрыть окно?")

                    pygame.draw.circle(screen, COLOR, (x_cir + gap, int(y_cir) - gap), int(FontSise / 12))  # =r
                    pygame.display.flip()

                    if x_cir + gap <= x_display // 12:
                        sprite = Circles(circles_group, x_cir + gap, int(y_cir) - gap, COLOR, int(FontSise / 12))
                        circles_group.add(sprite)
                        circles_group.draw(screen)
                        gun_group.update(degrees_a - 90, gap, y_display - gap)
                        gun_group.draw(screen)
                        screen.blit(wheel, (gap - 20, y_display - gap - 20))

                    if y_cir <= y_display and (x_cir <= x_display or s <= x_display):
                        y_cir = y_display - ((v * sin(a) * i) - (9.8 * i ** 2 / 2))
                        x_cir = int(v * cos(a) * i)
                    else:
                        while COLOR == a_color or (COLOR[0] > 200 and COLOR[1] > 200 and COLOR[2] > 200):
                            a_color = (randint(0, 255), randint(0, 255), randint(0, 255))
                        COLOR = a_color
                        indik = False
                        break

                if indik == False:
                    break

                clock.tick(20)
        mainLoopik = True
        write_text(FontSise, 'Больше графики чертить нельзя', (int(x_display // 50), 20), (255, 0, 0))
        while mainLoopik:
            for event in pygame.event.get():
                if event.type == QUIT:
                    raise SystemExit("Вы хотите закрыть окно?")
                if event.type == MOUSEBUTTONDOWN and continue_program(event.pos):
                        cord()


    def creating_a_coordinate_system():
        draw_axis([[5, y_display - gap], [x_display // 10, y_display - gap]])  # Ox
        draw_axis([[gap, y_display - 3 * gap], [gap, y_display + gap]])  # Oy

        Ox = [[5, y_display - gap], [x_display // 12, y_display - gap]]
        Oy = [[gap, y_display - 3 * gap], [gap, y_display + gap]]

        draw_point_zero()
        pygame.display.flip()

        draw_scale(Ox, Oy)


    def cord():
        global circles_group
        circles_group = pygame.sprite.Group()
        screen.fill(color_display)
        draw_axis([[5, y_display - gap], [x_display - 5, y_display - gap]])  # Ox
        draw_axis([[gap, 5], [gap, y_display + gap]])  # Oy

        Ox = [[5, y_display - gap], [x_display - 5, y_display - gap]]
        Oy = [[gap, 5], [gap, y_display + gap]]

        draw_point_zero()

        draw_arrow([[gap, x_display // 900], [gap - y_display // 90, x_display // 72]],
                   [[gap, x_display // 900], [gap + y_display // 90, x_display // 72]], 'h, м',
                   (FontSise // 4.5, FontSise // 1.5))

        draw_arrow([[x_display - x_display // 50, y_display - gap],
                    [x_display - x_display // 30, y_display - gap + y_display // 90]],
                   [[x_display - x_display // 50, y_display - gap],
                    [x_display - x_display // 30, y_display - gap - y_display // 90]], 'S, м',
                   (x_display - int(FontSise * 2.5), y_display - FontSise // 2))

        write_text(25, 'Клавиши управления:', (2 * gap, y_display + 80), (0, 0, 0))
        write_text(25,
                   'регулеровка скорости - "влево" и "вправо", регулировка угла - "вверх" и "вниз", начертить график - "Enter"',
                   (2 * gap, y_display + 100), (0, 0, 0))

        draw_scale(Ox, Oy)
        pygame.display.flip()

        # рисование графика
        make_the_circle_fly(x_display, y_display, color_display)


    mainLoop = True  # флаг окна
    while mainLoop:
        for event in pygame.event.get():
            if event.type == QUIT:
                mainLoop = False
        start_screen(x, y, color_display)
        #отображение системы коардинат и запуск программы
        cord()


except ValueError as e:
    mainLoop = True
    while mainLoop:
        for event in pygame.event.get():
            if event.type == QUIT:
                mainLoop = False
        screen.blit(
            pygame.font.SysFont("None", FontSise).render('Ошибка ввода, перезапустите программу', 0, (200, 0, 0)),
            (x_display // 4, y_display // 8))
        pygame.display.update()

pygame.quit()
